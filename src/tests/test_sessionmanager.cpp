#include "../core/connector.h"
#include "../core/queue.h"
#include "../core/dispatcher.h"
#include "../core/sessionmanager.h"
#include "../core/user.h"
using namespace botmaker;

#define USER_ID "userID_test"

int main()
{

    SessionManager *session = new SessionManager();
    using namespace std::this_thread;     // sleep_for, sleep_until
    using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
    using std::chrono::system_clock;
    botmaker::User *user = session->getSession(USER_ID);
    std::cout<<"Richiedo queue"<<endl;
    user->getQueue();
    sleep_for(2s);
    std::cout<<session->isSessionExpired(user)<<endl;
    sleep_for(10s);
    std::cout<<session->isSessionExpired(user)<<endl;
    session->eraseSession(USER_ID);
    return 0;
}