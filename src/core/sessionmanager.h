
#include <map>
#include <ctime>
#include <string>
#include "user.h"
#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H

namespace botmaker
{
#define SESSION_EXPIRATION_LIMIT 4 //seconds

    class SessionManager
    {
    private:
        std::map<std::string, User *> userList;
        void cleanSessions();
    public:
        SessionManager();
        bool isSessionExpired(User*);
        User *getSession(std::string);
        void runner();
        void eraseSession(std::string);
    };
}

#endif