#include "connector.h"

#define PERMS 0644

using namespace TgBot;

Connector::Connector(string token)
{
    cout<<"Connector created"<<endl;
    bot = new Bot(token);
}

void Connector::sendMessage(std::string message, std::string userID)
{
    bot->getApi().sendMessage(userID, message);
}

void Connector::runner()
{
    cout<<"Connector starting polling telegram server"<<endl;
    Queue *localInputQueue = inputQueue;
    bot->getEvents().onAnyMessage([localInputQueue](Message::Ptr message)
                                    {
                                        MessageInputType *messageBuf = new MessageInputType();
                                        messageBuf->mtype = 1;
                                        messageBuf->message = message;
                                        localInputQueue->sendMessage(messageBuf);
                                    });
    TgLongPoll longPoll(*bot);
    while (true)
    {
        longPoll.start();
    }
}

void Connector::setQueue(Queue *queue)
{
    inputQueue=queue;
}