#include <tgbot/tgbot.h>

#ifndef MESSAGEINPUTTYPE_H // include guard
#define MESSAGEINPUTTYPE_H

namespace botmaker
{
    struct MessageInputType
    {
        long mtype;                  /* message type, must be > 0 */
        TgBot::Message::Ptr message; /* message data */
    };
}
#endif
