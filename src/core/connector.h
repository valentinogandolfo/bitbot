#include <string>
#include <tgbot/tgbot.h>
#include "queue.h"

#ifndef CONNECTOR_H // include guard
#define CONNECTOR_H

using namespace std;
using namespace TgBot;
namespace botmaker
{
    class Connector
    {
    private:
        Queue *inputQueue;
        Bot *bot;
        key_t key;

    public:
        Connector(string token);
        void sendMessage(string message, string userID);
        void runner();
        void setQueue(Queue *queue);
    };
}
#endif