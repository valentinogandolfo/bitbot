#include <sys/types.h>
#include <tgbot/tgbot.h>
#include "messageinputtype.h"
#include <ctime>
#include <iostream>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string>
#include <stdlib.h>
#include <errno.h>
using namespace botmaker;
#define QUEUE_PERMS 0644

#ifndef QUEUE_H // include guard
#define QUEUE_H

namespace botmaker
{

    class Queue
    {
    private:
        key_t key;
        int queueID;

    public:
        Queue();
        TgBot::Message::Ptr getMessage();
        void sendMessage(MessageInputType *message);
    };
}
#endif