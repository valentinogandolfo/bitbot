#include "queue.h"
#include "iostream"

#ifndef DISPATCHER_H // include guard
#define DISPATCHER_H

namespace botmaker
{
    class Dispatcher
    {
    private:
        Queue *inputQueue;

    public:
        Dispatcher();
        void setQueue(Queue *queue);
        void runner();
    };
    
}
#endif