#include "sessionmanager.h"
using namespace botmaker;

SessionManager::SessionManager()
{
}

bool SessionManager::isSessionExpired(User *user)
{
    return std::time(nullptr) - user->getLastHit() >= SESSION_EXPIRATION_LIMIT;
}

User *SessionManager::getSession(std::string userID)
{
    User *userFound = NULL;
    userFound = userList[userID];

    if (userFound == NULL)
    {
        userFound = userList[userID] = new User(userID);
    }
    return userFound;
}

void SessionManager::runner() //TODO
{
}

void SessionManager::eraseSession(std::string userID)//TODO
{
    User *userFound = NULL;
    userFound = userList[userID];
    userList.erase(userID);
    delete userFound;
}