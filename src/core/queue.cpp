#include "queue.h"

using namespace std;
Queue::Queue()
{
    time_t timestamp = time(nullptr);
    string filename = asctime(localtime(&timestamp));

    if(fopen(filename.c_str(),"w+")==NULL){
        perror("fopen: ");
        exit(-1);
    }
    if ((key = ftok(filename.c_str(), 'P')) == -1)
    {
        perror("ftok");
        exit(1);
    }

    if ((queueID = msgget(key, QUEUE_PERMS | IPC_CREAT)) == -1)
    {
        perror("msgget");
        exit(1);
    }
}

TgBot::Message::Ptr Queue::getMessage()
{
        MessageInputType *message = new MessageInputType();
        int length = sizeof(MessageInputType) - sizeof(long);

        if (msgrcv(queueID, message, length, 1, 0) == -1)
        {
            perror("msgrcv");
            exit(1);
        }

        return message->message;
}

void Queue::sendMessage(MessageInputType *message)
{
    int length = sizeof(MessageInputType) - sizeof(long);
    if (msgsnd(queueID, (void *)message, length, 0) == -1) /* +1 for '\0' */
        perror("msgsnd");
}