#include "user.h"

User::User(std::string _userID)
{
    userID = _userID;
    hit();
    queue =  std::make_unique<botmaker::Queue>();
}

std::string User::getUserID()
{
    return userID;
}

time_t User::getLastHit()
{
    return lastHit;
}

void User::hit()
{
    lastHit = std::time(nullptr);
}

botmaker::Queue botmaker::User::getQueue(){
    return *queue;
}