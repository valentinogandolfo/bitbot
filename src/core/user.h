#include <ctime>
#include <string>
#include "queue.h"

#ifndef USER_H
#define USER_H

namespace botmaker
{
    class User
    {
    private:
        std::string userID;
        time_t lastHit;
        std::unique_ptr<botmaker::Queue> queue;

    public:
        User(std::string _userID);
        void hit();
        time_t getLastHit();
        std::string getUserID();
        botmaker::Queue getQueue();
    };
}

#endif